<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<%@include file="/WEB-INF/views/common/head.jsp"%>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <%@ include file="/WEB-INF/views/common/header.jsp" %>
    <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="/WEB-INF/views/common/sidebar.jsp" %>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clients
                <a class="btn btn-primary" href="${pageContext.request.contextPath}/users/create">Ajouter</a>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Nom</th>
                                    <th>Pr&eacute;nom</th>
                                    <th>Email</th>
                                    <!-- 13/11/19 7.1 : ajout en-tête date naissance -->
                                    <th>Naissance</th>
                                    <th>Action</th>
                                </tr>
                                <c:forEach items="${listeClients}" var="client">
                                    <tr>
                                        <td>${client.id}</td>
                                        <td>${client.nom}</td>
                                        <td>${client.prenom}</td>
                                        <td>${client.email}</td>
                                        <td>${client.naissance}</td>
                                        <td><!-- 13/11/19 7.1 activation des boutons -->
                                            <a class="btn btn-primary " href="${pageContext.request.contextPath }/users/list?selectedClientDetails=${client.id}"> <i class="fa fa-play"></i></a>
                                            <a class="btn btn-success" href="${pageContext.request.contextPath }/users/list?selectedClientUpdate=${client.id}"> <i class="fa fa-edit"></i></a>
                                            <!-- EDIT 04/11 : on active le bouton
                                            <a class="btn btn-danger disabled" href="#"> <i class="fa fa-trash"></i></a>
                                            -- Fin de modif -->
                                            <a class="btn btn-danger" href="${pageContext.request.contextPath}/users/list?selectedClient=${client.id}" onclick="return confirm('Cette action supprimera le client et, le cas &eacute;ch&eacute;ant, toutes les voitures dont il est propri&eacute;taire et toutes les r&eacute;servations le concernant.  Voulez-vous continuer ?')"> <i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
    </div>

    <%@ include file="/WEB-INF/views/common/footer.jsp" %>
</div>
<!-- ./wrapper -->

<%@ include file="/WEB-INF/views/common/js_imports.jsp" %>

<!-- EDIT 05/11 : ajout de messages quand ça va bien et quand ça plante > renvoi à la couche Contrôleur -->

<%@ include file="/WEB-INF/views/common/error.jsp" %>

</body>
</html>
