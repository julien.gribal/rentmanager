<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<%@include file="/WEB-INF/views/common/head.jsp"%>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <%@ include file="/WEB-INF/views/common/header.jsp" %>
    <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="/WEB-INF/views/common/sidebar.jsp" %>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <h3 class="profile-username text-center">R&eacute;servation ${reservation.id}</h3>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    
                     <table class="table table-striped">
                         <tr>
                             <td><b>Client:</b></td>
                             <td>${reservation.client.prenom} ${reservation.client.nom} (${reservation.client.email})</td>
                         </tr>
                        	<tr>
                             <td><b>Date de d&eacute;but :</b></td>
                             <td>${reservation.debut}</td>
                         </tr>
                         <tr>
                             <td><b>Date de fin :</b></td>
                             <td>${reservation.fin}</td>
                         </tr>
                         <tr>
                             <td><b>Voiture :</b></td>
                             <td>${reservation.vehicle.constructeur} ${reservation.vehicle.modele} (${reservation.vehicle.nb_places} pl.)</td>
                         </tr>
                         <tr>
                             <td><b>Propri&eacute;taire:</b></td>
                             <td>${reservation.vehicle.client.prenom} ${reservation.vehicle.client.nom} (${reservation.vehicle.client.email})</td>
                         </tr>
                     </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>

    <%@ include file="/WEB-INF/views/common/footer.jsp" %>
</div>
<!-- ./wrapper -->

<%@ include file="/WEB-INF/views/common/js_imports.jsp" %>
   <%@ include file="/WEB-INF/views/common/error.jsp" %>
</body>
</html>
