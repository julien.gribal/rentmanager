<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<%@include file="/WEB-INF/views/common/head.jsp"%>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <%@ include file="/WEB-INF/views/common/header.jsp" %>
    <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="/WEB-INF/views/common/sidebar.jsp" %>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Voitures
                <a class="btn btn-primary" href="${pageContext.request.contextPath}/vehicles/create">Ajouter</a>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Marque</th>
                                    <th>Mod&egrave;le</th>
                                    <th>Nombre de places</th>
                                    <th>Propri&eacute;taire</th>
                                    <th>Action</th>
                                </tr>
                                <c:forEach items="${listeVehicles}" var="vehicule">
   									<tr>
     									<td>${vehicule.id}</td>
     									<td>${vehicule.constructeur}</td>
     									<td>${vehicule.modele}</td>
     									<td>${vehicule.nb_places}</td>
                                        <!-- Projet 13/11 -->
                                        <td>${vehicule.client.prenom} ${vehicule.client.nom}</td>
                                        <!-- Projet 13/11 -->
     									<td><!-- 13/11/19 7.1 : activation des boutons -->
                                            <a class="btn btn-primary" href="${pageContext.request.contextPath }/vehicles/list?selectedVehicleDetails=${vehicule.id}"> <i class="fa fa-play"></i></a>
                                            <a class="btn btn-success" href="${pageContext.request.contextPath }/vehicles/list?selectedVehicleUpdate=${vehicule.id}"> <i class="fa fa-edit"></i></a>
                                            <!-- EDIT 04/11 : on active le bouton :
     										<a class="btn btn-danger disabled" href="#"> <i class="fa fa-trash"></i></a>
     										  Fin EDIT -->
                                            <a class="btn btn-danger" href="${pageContext.request.contextPath}/vehicles/list?selectedVehicule=${vehicule.id}" onclick="return confirm('Cette action supprimera la voiture et, le cas &eacute;ch&eacute;ant, toutes les r&eacute;servations associ&eacute;es &agrave; celle-ci.  Voulez-vous continuer ?')"> <i class="fa fa-trash"></i></a>
                                        </td>
     								</tr>
  								</c:forEach>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
    </div>

    <%@ include file="/WEB-INF/views/common/footer.jsp" %>
</div>
<!-- ./wrapper -->

<%@ include file="/WEB-INF/views/common/js_imports.jsp" %>

<!-- Projet 13/11 -->
<!-- Ajout de scripts pour message Growl afin d'informer l'utilisateur en cas de succès ou échec -->
<%@ include file="/WEB-INF/views/common/error.jsp" %>

</body>
</html>
