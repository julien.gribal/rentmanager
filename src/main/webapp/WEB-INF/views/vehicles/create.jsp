<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<%@include file="/WEB-INF/views/common/head.jsp"%>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <%@ include file="/WEB-INF/views/common/header.jsp" %>
    <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="/WEB-INF/views/common/sidebar.jsp" %>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Voitures
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box">
                        <!-- form start -->
                        <!-- Le  type de methode http qui sera appelé lors de action submit du formulaire -->
                        <!-- est décrit an l'attribut "method" de la balise form -->
                        <!-- action indique à quelle "cible" sera envoyée la requête, ici notre Servlet qui sera bind sur -->
                        <!-- /vehicles/create -->
                        <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/vehicles/create">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="brand" class="col-sm-2 control-label">Marque</label>
									<!-- Pour récupérer la valeur rentrée dans un champ input de cette jsp au niveau de votre servlet -->
									<!-- vous devez passer par les methodes getParameter de l'objet request, et spécifiant la valeur -->
									<!-- de l'attribut "name" de l'input -->
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="brand" name="constructeur" placeholder="Marque" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="model" class="col-sm-2 control-label">Mod&egrave;le</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="model" name="modele" placeholder="Modele" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seats" class="col-sm-2 control-label">Nombre de places</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="seats" name="nb_places" placeholder="Nombre de places" required>
                                    </div>
                                </div>
                                <!-- 13/11/19 7.1 Ajout liste propriétaires -->
                                <div class="form-group">
                                    <label for="client" class="col-sm-2 control-label">Propri&eacute;taire</label>

                                    <div class="col-sm-10">
                                        <select class="form-control" id="client" name="client.id">
                                            <c:forEach items="${listeClients}" var="client">
                                                <option value="${client.id}">${client.prenom} ${client.nom} (${client.email})</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Ajouter</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
    </div>

    <%@ include file="/WEB-INF/views/common/footer.jsp" %>
</div>
<!-- ./wrapper -->

<%@ include file="/WEB-INF/views/common/js_imports.jsp" %>

<!-- Projet 13/11 -->
<!-- Ajout de scripts pour messages Growl afin d'informer l'utilisateur en cas de succès ou échec -->
<%@ include file="/WEB-INF/views/common/error.jsp" %>

</body>
</html>
