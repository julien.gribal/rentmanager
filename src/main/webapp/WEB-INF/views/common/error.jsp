<!-- Projet 13/11 -->
<!-- Ajout de scripts pour message Growl afin d'informer l'utilisateur en cas de succès ou échec -->
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<c:if test="${not empty message}">
	<script>
		$.growl({ title: "Information", message: "${message}"});
	</script>
</c:if>
<c:if test="${not empty errorMessage}">
	<script>
		$.growl.error({ title: "Erreur", message: "${errorMessage}"});
	</script>
</c:if>