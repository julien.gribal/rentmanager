<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVIGATION</li>
            <!-- 7.1 13/11 : ajout d'un raccourci 'home' pour faciliter la nav' -->
            <li>
                <a href="${pageContext.request.contextPath}/home">
                    <i class="fa fa-home"></i> <span>Accueil</span>
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/users/list">
                    <i class="fa fa-user"></i> <span>Clients</span>
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/vehicles/list">
                    <i class="fa fa-car"></i> <span>Voitures</span>
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/rents/list">
                    <i class="fa fa-pencil"></i> <span>R&eacuteservations</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>