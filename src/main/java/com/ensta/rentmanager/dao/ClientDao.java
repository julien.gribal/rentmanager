
package com.ensta.rentmanager.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.mapper.ClientMapper;
import com.ensta.rentmanager.model.Client;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class ClientDao {


	// TODO: Bonus: implementer le design pattern singleton pour cette classe

	private final Session session;
	private final ClientMapper clientMapper;
	

	public ClientDao(Session session, ClientMapper clientMapper) {
		this.session = session;
		this.clientMapper = clientMapper;
	}

	public long create(ClientDto clientDto) throws DaoException, SQLException {
		Client client = clientMapper.mapToEntity(clientDto);
		session.save(client);
		System.out.println("Client créé:"+client.toString());
		session.beginTransaction().commit();
		return client.getId();

	}

	public void delete(int id) throws DaoException, SQLException {
		
		Client  client = clientMapper.mapToEntity(findById(id));
		session.clear();
		Object managed = session.merge(client);
		session.remove(managed);
		System.out.println("Client supprimé:"+client.toString());
		session.beginTransaction().commit();
	

	}

	
	public ClientDto findById(int id) throws DaoException, SQLException {
		
		Client client = session.find(Client.class,(int) id);
		return clientMapper.mapToDto(client) ;
	}

	public List<ClientDto> findAll() throws DaoException, SQLException {
			List<ClientDto> listeClientDto = new ArrayList<ClientDto>();
			Query query =  session.createQuery("SELECT c FROM Client c");
			@SuppressWarnings("unchecked")
			List<Client> res = (List<Client>) query.getResultList();
			
			for (Client client : res) {
				listeClientDto.add(clientMapper.mapToDto(client));
			}
			
			return listeClientDto;
	}

	public int count() throws SQLException {

		Query query =  session.createQuery("SELECT count(c) FROM Client c");
		int nb =  Integer.parseInt(query.getSingleResult().toString());

		return nb;

	}

	//Bonus update client
	public long update(ClientDto clientDto) {
		Client client = clientMapper.mapToEntity(clientDto);
		Object managed = session.merge(client);
		session.update(managed);
		System.out.println("Client mis à jour:"+client.toString());
		session.beginTransaction().commit();
		return client.getId();
	}

}
