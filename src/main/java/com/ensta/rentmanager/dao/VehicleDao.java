package com.ensta.rentmanager.dao;

import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.mapper.VehicleMapper;
import com.ensta.rentmanager.model.Vehicle;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleDao {

    private final Session session;
    private final VehicleMapper vehicleMapper;

    public VehicleDao(Session session, VehicleMapper vehicleMapper) {
        this.session = session;
        this.vehicleMapper = vehicleMapper;
    }

    public long create(VehicleDto vehicleDto) throws DaoException, SQLException, ServiceException {
        
    	Vehicle vehicle = vehicleMapper.mapToEntity(vehicleDto);
    	session.save(vehicle);
    	System.out.println("Véhicule créé:"+vehicle.toString());
    	session.beginTransaction().commit();
        return vehicle.getId();
        
    }

    public void delete(int id) throws DaoException, SQLException, ServiceException {
        
    	Vehicle vehicle = vehicleMapper.mapToEntity(findById(id));
    	session.clear();
    	Object managed = session.merge(vehicle);
		session.remove(managed);
		System.out.println("Véhicule supprimé:"+vehicle.toString());
		session.beginTransaction().commit();
    }

    public VehicleDto findById(long id) throws DaoException, SQLException, ServiceException {
        VehicleDto vehicleDto = vehicleMapper.mapToDto(session.find(Vehicle.class, (int) id));
        return vehicleDto;
    }

    public List<VehicleDto> findAll() throws DaoException, SQLException, ServiceException {
    	
    	List<VehicleDto> listeVehicleDto = new ArrayList<VehicleDto>()
;        
            Query query = session.createQuery("SELECT v FROM Vehicle v");
            @SuppressWarnings("unchecked")
			List<Vehicle> res = (List<Vehicle>) query.getResultList();
            
            for (Vehicle vehicle : res) {
            	listeVehicleDto.add(vehicleMapper.mapToDto(vehicle));
            }
            return listeVehicleDto;
        
    }

    public int count() throws SQLException {
    	Query query = session.createQuery("SELECT count(v) FROM Vehicle v");
        int nb = Integer.parseInt(query.getSingleResult().toString());
        return nb;

    }

	public List<VehicleDto> findByProprietaireId(int clientId) throws ServiceException, DaoException, SQLException {
		List<VehicleDto> listeVehicleDto = new ArrayList<VehicleDto>();
		
		Query query =  session.createQuery("SELECT v FROM Vehicle v WHERE v.client.id = :clientId", Vehicle.class).setParameter("clientId",clientId);
		@SuppressWarnings("unchecked")
		List<Vehicle> res = (List<Vehicle>) query.getResultList();
		
		for (Vehicle vehicle : res) {
			listeVehicleDto.add(vehicleMapper.mapToDto(vehicle));
		}
		return listeVehicleDto;
		
	}

	//Bonus update vehicule
		public long update(VehicleDto vehicleDto) throws ServiceException, DaoException, SQLException {
			Vehicle vehicle = vehicleMapper.mapToEntity(vehicleDto);
			Object managed = session.merge(vehicle);
			session.update(managed);
			System.out.println("Véhicule mise à jour:"+vehicle.toString());
			session.beginTransaction().commit();
			return vehicle.getId();
		}

}
