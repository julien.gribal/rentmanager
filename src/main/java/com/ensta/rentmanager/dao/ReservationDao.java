package com.ensta.rentmanager.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.mapper.ReservationMapper;
import com.ensta.rentmanager.model.Reservation;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class ReservationDao {

	private final Session session;
	private final ReservationMapper reservationMapper;

	public ReservationDao(Session session, ReservationMapper reservationMapper) {
		this.session = session;
		this.reservationMapper = reservationMapper;
	}

	public int create(ReservationDto reservationDto) throws DaoException, ServiceException, SQLException {
		Reservation reservation = reservationMapper.mapToEntity(reservationDto);
		session.save(reservation);
		System.out.println("Réservation créée:"+reservation.toString());
		session.beginTransaction().commit();
		return reservation.getId();
	}

	public void delete(int id) throws DaoException, ServiceException, SQLException {
		Reservation reservation = reservationMapper.mapToEntity(findById(id));
		session.clear();
		Object managed = session.merge(reservation);
		session.remove(managed);
		System.out.println("Réservation supprimée:"+reservation.toString());
		session.beginTransaction().commit();

	}

	public List<ReservationDto> findResaByClientId(int clientId) throws ServiceException, DaoException, SQLException  {
		List<ReservationDto> listeReservationDto = new ArrayList<ReservationDto>();
		
		Query query =  session.createQuery("SELECT r FROM Reservation r WHERE r.client.id = :clientId", Reservation.class).setParameter("clientId",clientId);
		@SuppressWarnings("unchecked")
		List<Reservation> res = (List<Reservation>) query.getResultList();
		
		for (Reservation reservation : res) {
			listeReservationDto.add(reservationMapper.mapToDto(reservation));
		}
		
		return listeReservationDto;

	}

	public List<ReservationDto> findResaByVehicleId(int vehicleId) throws DaoException, ServiceException, SQLException {
		
		List<ReservationDto> listeReservationDto = new ArrayList<ReservationDto>();
		
			Query query =  session.createQuery("SELECT r FROM Reservation r WHERE r.vehicle.id = :vehicleId", Reservation.class).setParameter("vehicleId",vehicleId);
		
			@SuppressWarnings("unchecked")
			List<Reservation> res = (List<Reservation>) query.getResultList();
			
			for (Reservation reservation : res) {
				listeReservationDto.add(reservationMapper.mapToDto(reservation));
			}
			
			return listeReservationDto;

	} 

	public List<ReservationDto> findAll() throws DaoException {
		
			List<ReservationDto> listeReservationDto = new ArrayList<ReservationDto>();
		
			Query query =  session.createQuery("SELECT r FROM Reservation r");
			@SuppressWarnings("unchecked")
			List<Reservation> res = (List<Reservation>) query.getResultList();
			
			for (Reservation reservation : res) {
				listeReservationDto.add(reservationMapper.mapToDto(reservation));
			}
			
			return listeReservationDto;

	}

	public int count() {
		Query query =  session.createQuery("SELECT count(r) FROM Reservation r");
		int nb =  Integer.parseInt(query.getSingleResult().toString());

		return nb;
	}
	
	public ReservationDto findById(int id) {
		
		Reservation reservation = session.find(Reservation.class, id);
		return reservationMapper.mapToDto(reservation);
	}
	
	//Projet 13/11 update resa
	public long update(ReservationDto reservationDto) throws ServiceException, DaoException, SQLException {
		Reservation reservation = reservationMapper.mapToEntity(reservationDto);
		Object managed = session.merge(reservation);
		session.update(managed);
		System.out.println("Réservation mise à jour:"+reservation.toString());
		session.beginTransaction().commit();
		return reservation.getId();
	}
}
