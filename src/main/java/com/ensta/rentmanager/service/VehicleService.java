package com.ensta.rentmanager.service;

import java.sql.SQLException;
import java.util.List;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.exception.ValidationFonctionnelleException;
import com.ensta.rentmanager.dao.VehicleDao;
import com.ensta.rentmanager.dto.VehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleService {

	private VehicleDao vehicleDao;

	@Autowired
	public  VehicleService(VehicleDao vehicleDao) {
		this.vehicleDao = vehicleDao;
	}
	
	public long create(VehicleDto vehicleDto) throws ServiceException, DaoException, SQLException {
		
		if ( ( vehicleDto.getConstructeur() == null || ( vehicleDto.getConstructeur().equalsIgnoreCase("") ) && vehicleDto.getNb_places() > 1 ) ) {
			throw new ServiceException("Le constructeur doit être renseigné le nombre de places doit être supérieur à 1.") ;
		}
		else {
			return vehicleDao.create(vehicleDto) ;
		}
	}

	public VehicleDto findById(long id) throws ServiceException, DaoException, SQLException {
		if ( id == 0 ) { // contr�le bancal parce qu'un long n'est jamais nullable et qu'une table peut commencer avec l'ID 0
			throw new ServiceException() ;
		}
		else {
			VehicleDto vehicle_trouve = vehicleDao.findById(id) ;
			return vehicle_trouve;
		}
	}

	public List<VehicleDto> findAll() throws ServiceException, DaoException, SQLException {
		
		List<VehicleDto> listeDto = vehicleDao.findAll() ;
		return listeDto;
	
	}
	
	public int count() throws SQLException, DaoException, SQLException {
		
		int compteur = vehicleDao.count() ;
		return compteur;
		
	}
	
	public void delete(int id) throws DaoException, SQLException, ServiceException {
		vehicleDao.delete(id) ;
		
	}
	
	public List<VehicleDto> findByProprietaireId(int id) throws ServiceException, DaoException, SQLException{
	
		List<VehicleDto> listeDto = vehicleDao.findByProprietaireId(id);
	
		return listeDto;
	}

	//Bonus update vehicule
	public long update(VehicleDto vehicleDto) throws ServiceException, ValidationFonctionnelleException, DaoException, SQLException {
		
			return vehicleDao.update(vehicleDto);
		
	}
	
	
}
