package com.ensta.rentmanager.service;

import java.sql.SQLException;
import java.util.List;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.exception.ValidationFonctionnelleException;
import com.ensta.rentmanager.utils.IOUtils;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dto.ClientDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

	private ClientDao clientDao;

	@Autowired
	public  ClientService(ClientDao clientDao) {
		this.clientDao = clientDao;
	}
	
	public long create(ClientDto clientDto) throws ServiceException, DaoException, SQLException, ValidationFonctionnelleException {

		if ( clientDto.getNom() == null || clientDto.getNom().equalsIgnoreCase("") || clientDto.getPrenom() == null || clientDto.getPrenom().equalsIgnoreCase("") ) {
			throw new ServiceException("Le nom ou le prénom sont des valeurs obligatoires") ;
		}
		
		//Verif Age
		else if(IOUtils.calculAge(IOUtils.readDate(clientDto.getNaissance()))<18) throw new ValidationFonctionnelleException("Le client doit avoir plus de 18 ans");
		
		else
			clientDto.setNom(clientDto.getNom().toUpperCase()) ;
		    
			return clientDao.create(clientDto);
	}

	public ClientDto findById(int id) throws ServiceException, DaoException, SQLException {
		if ( id == 0 ) { 
			throw new ServiceException() ;
		}
		else {
			ClientDto client_trouve = clientDao.findById(id) ;
			return client_trouve;
		}
		
	}

	public List<ClientDto> findAll() throws ServiceException, DaoException, SQLException {
		
		List<ClientDto> listeClientDto = clientDao.findAll() ;
		return listeClientDto;
	
	}

	public void delete(int id) throws ServiceException, DaoException, SQLException {
		clientDao.delete(id) ;
	}

	public int count() throws SQLException, DaoException, SQLException {

		int compteur = clientDao.count() ;
		return compteur;


	}
	
	//Bonus update client
	public long update(ClientDto clientDto) throws ServiceException, ValidationFonctionnelleException {
		
		
		if ( clientDto.getNom() == null || clientDto.getNom().equalsIgnoreCase("") || clientDto.getPrenom() == null || clientDto.getPrenom().equalsIgnoreCase("") ) {
			throw new ServiceException("Le nom ou le prénom sont des valeurs obligatoires") ;
		}
		
		else if(IOUtils.calculAge(IOUtils.readDate(clientDto.getNaissance()))<18) throw new ValidationFonctionnelleException("Le client doit avoir plus de 18 ans");
		
		else
			clientDto.setNom(clientDto.getNom().toUpperCase());
			return clientDao.update(clientDto);
		
	}

}
