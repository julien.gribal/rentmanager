package com.ensta.rentmanager.service;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ensta.rentmanager.dao.ReservationDao;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.exception.ValidationFonctionnelleException;
import com.ensta.rentmanager.utils.IOUtils;


@Service
public class ReservationService {

    private ReservationDao reservationDao;

    @Autowired
    public  ReservationService(ReservationDao reservationDao) {
        this.reservationDao = reservationDao;
    }

    public long create(ReservationDto reservationDto) throws ServiceException, DaoException, SQLException, ValidationFonctionnelleException {

    	//Verifs règles métier
    	//Champs absents	
        if ( reservationDto.getClient() == null || reservationDto.getVehicle() == null ) {
            throw new ServiceException("Tous les champs doivent être renseignés") ;
        }
        //Verif Begda >= Endda
        else if (!IOUtils.verifZ(reservationDto.getDebut(), reservationDto.getFin())) throw new ValidationFonctionnelleException("Begda >= Endda");
        
        else {
            return reservationDao.create(reservationDto) ;
        }
    }

    public List<ReservationDto> findResaByClientId(int id) throws ServiceException, DaoException, SQLException {
        if ( id == 0 ) {
            throw new ServiceException() ;
        }
        else {
            List<ReservationDto> reservation_trouve = reservationDao.findResaByClientId(id) ;
            return reservation_trouve;
        }
    }

    public List<ReservationDto> findResaByVehicleId(int id) throws ServiceException, DaoException, SQLException {
        if ( id == 0 ) { 
            throw new ServiceException() ;
        }
        else {
            List<ReservationDto> reservation_trouve = reservationDao.findResaByVehicleId(id) ;
            return reservation_trouve;
        }
    }


    public List<ReservationDto> findAll() throws ServiceException, DaoException, SQLException {
    	List<ReservationDto> reservation_trouve = reservationDao.findAll() ;
        return reservation_trouve;

    }


    public int count() throws SQLException, DaoException, SQLException {

        int compteur = reservationDao.count() ;
        return compteur;

    }
    
 
    public void delete(int id) throws DaoException, ServiceException, SQLException {
    	reservationDao.delete(id);
    }

    //Ajout Projet 13/11 - c'était pas prévu!!! mais utile pour la page "details" des réservations
	public ReservationDto findById(int id) {
		ReservationDto reservation = reservationDao.findById(id) ;
		return reservation;
	
	}

	public long update(ReservationDto reservationDto) throws ServiceException, DaoException, SQLException, ValidationFonctionnelleException {
		
		//Verif Begda >= Endda
        if (!IOUtils.verifZ(reservationDto.getDebut(), reservationDto.getFin())) throw new ValidationFonctionnelleException("Begda >= Endda");
        
		return reservationDao.update(reservationDto);
		
	}



}
