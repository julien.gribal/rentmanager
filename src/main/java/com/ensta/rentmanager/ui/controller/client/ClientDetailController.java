package com.ensta.rentmanager.ui.controller.client;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;


//Projet 13/11
@Controller
@RequestMapping("users/details")
public class ClientDetailController {
	
	private ClientService clientService;
	private ReservationService reservationService;
	private VehicleService vehicleService;

    public ClientDetailController(VehicleService vehicleService, ReservationService reservationService, ClientService clientService) {
        this.clientService = clientService;
        this.reservationService = reservationService;
        this.vehicleService = vehicleService;

    }
    
    
    @GetMapping
    public ModelAndView get(@ModelAttribute("clientDetailId") final int id) {
    	
    	try {
    		
			ClientDto clientDto = clientService.findById(id);
			List<ReservationDto> listeReservationDto = reservationService.findResaByClientId(id); 
			List<VehicleDto> listeVehicleDto = vehicleService.findByProprietaireId(id); 
			
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("users/details");
	        outputModelAndView.addObject("listeReservation",listeReservationDto );
	        outputModelAndView.addObject("listeVehicule",listeVehicleDto );
	        outputModelAndView.addObject("client",clientDto );
	        
	        
	        return outputModelAndView;

			
		} catch (Exception e) {
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("users/details");
	        outputModelAndView.addObject("errorMessage", "Problème de récupération des détails");
	        e.printStackTrace();
	        return outputModelAndView;
			
		} 
    	
    }
    
    

}