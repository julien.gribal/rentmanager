package com.ensta.rentmanager.ui.controller.reservation;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.service.ReservationService;


@Controller
@RequestMapping("/rents/list")
public class ReservationListController {

    private ReservationService reservationService;

    public ReservationListController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    //Projet13/11
    //Ajout d'infos à l'utilisateur en cas de problème de requête findAll()
    @GetMapping
    public ModelAndView get()  {

    	ModelAndView outputModelAndView = new ModelAndView();
        
		try {
			List<ReservationDto> listeReservationDto = reservationService.findAll();
			outputModelAndView.setViewName("rents/list");
	        outputModelAndView.addObject("listeReservations", listeReservationDto);
	        return outputModelAndView;
	        
		} catch (Exception e) {
			outputModelAndView.setViewName("rents/list");
			//Message d'info
	        outputModelAndView.addObject("errorMessage", "Problème de récupération de la liste des réservations");
	        e.printStackTrace();
	        return outputModelAndView;
		} 

    }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id du client" afin de pouvoir le supprimer
    @RequestMapping(method = RequestMethod.GET, params = {"selectedReservation"})
    public String delete(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedReservation", required = false) int selectedReservation) {

        try {
        	//Tentative de suppression
			reservationService.delete(selectedReservation);
			//Information de succès - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("message", "Réservation n°" + selectedReservation + " supprimée!");
			//Retour sur la page 
			return "redirect:/rents/list";
		} catch (Exception e) {
			//Catch de toute erreur
			 //Information de succès - ajout d'un attribut à la vue
			 redirectAttributes.addFlashAttribute("errorMessage", "Un problème est survenu lors de la suppression de la réservation n°"+selectedReservation+" !");
			 e.printStackTrace();
			 //Retour sur la page 
			 return "redirect:/rents/list";
		}

    }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id de la reservation" afin d'avoir les détails
    @RequestMapping(method = RequestMethod.GET, params = {"selectedReservationDetails"})
    public String showDetails(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedReservationDetails", required = false) int selectedReservationDetails) {
    	
    	  	redirectAttributes.addFlashAttribute("reservationDetailId", selectedReservationDetails);
    	  	return "redirect:/rents/details";
		
       
   }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id du véhicule" afin de faire un update
    @RequestMapping(method = RequestMethod.GET, params = {"selectedReservationUpdate"})
    public String update(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedReservationUpdate", required = false) int selectedReservationUpdate) {
    	
    	  	redirectAttributes.addFlashAttribute("selectedReservationId", selectedReservationUpdate);
    	  	return "redirect:/rents/update";
		
   }


}
