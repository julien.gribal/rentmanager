package com.ensta.rentmanager.ui.controller.client;

import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.service.ClientService;

@Controller
@RequestMapping("/users/list")
public class ClientListController {

    private final ClientService clientService;


    public ClientListController(ClientService clientService) {
        this.clientService = clientService;
    }
    
    @GetMapping
    public ModelAndView get() {

        ModelAndView outputModelAndView = new ModelAndView();
		
        try {
			List<ClientDto> listeClientDto = lireClients();
			
			outputModelAndView.setViewName("users/list");
	        outputModelAndView.addObject("listeClients", listeClientDto);
	        return outputModelAndView;
	        
		} catch (Exception e) {
			outputModelAndView.setViewName("users/list");
			//Message d'erreur
			outputModelAndView.addObject("errorMessage", "Problème de récupération de la liste des Clients");
	        e.printStackTrace();
	        return outputModelAndView;
		} 
    }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id du client" afin de pouvoir le supprimer
    @RequestMapping(method = RequestMethod.GET, params = {"selectedClient"})
    public String delete(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedClient", required = false) int selectedClient) {
    	
        try {
        	//Tentative de suppression
			clientService.delete(selectedClient);
			//Information de succès - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("message", "Client n°" + selectedClient + " supprimé!");
			//Retour sur la page  
			return "redirect:/users/list";
		} catch (Exception e) {
			 //Catch de toute erreur
			 //Information de succès - ajout d'un attribut à la vue
			 redirectAttributes.addFlashAttribute("errorMessage", "Un problème est survenu lors de la suppression du client n°"+selectedClient+" !");
			 e.printStackTrace();
			 //Retour sur la page 
			 return "redirect:/users/list";
		} 
       
   }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id du client" afin d'avoir les détails
    @RequestMapping(method = RequestMethod.GET, params = {"selectedClientDetails"})
    public String showDetails(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedClientDetails", required = false) int selectedClientDetails) {
    	
    	  	redirectAttributes.addFlashAttribute("clientDetailId", selectedClientDetails);
    	  	return "redirect:/users/details";
		
       
   }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id du client" afin de faire un update
    @RequestMapping(method = RequestMethod.GET, params = {"selectedClientUpdate"})
    public String update(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedClientUpdate", required = false) int selectedClientUpdate) {
    	
    	  	redirectAttributes.addFlashAttribute("clientUpdateId", selectedClientUpdate);
    	  	return "redirect:/users/update";
		
   }
    
    
    private  List<ClientDto> lireClients() throws ServiceException, DaoException, SQLException {

        List<ClientDto> listClientDto = clientService.findAll() ;
        return listClientDto;

    }


}
