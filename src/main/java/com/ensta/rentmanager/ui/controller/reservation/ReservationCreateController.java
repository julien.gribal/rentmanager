package com.ensta.rentmanager.ui.controller.reservation;

import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.exception.ValidationFonctionnelleException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@Controller
@RequestMapping("rents/create")
public class ReservationCreateController {

    private ReservationService reservationService;
    private VehicleService vehicleService;
    private ClientService clientService;

    public ReservationCreateController(ReservationService reservationService, VehicleService vehicleService, ClientService clientService) {
        this.reservationService = reservationService;

        this.vehicleService = vehicleService;
        this.clientService = clientService;

    }

    //Projet 13/11
    //Message d'eereur en cas de pb pour récupérer les véhicules ou clients
    @GetMapping
    public ModelAndView get()  {
    	 
    	ModelAndView outputModelAndView = new ModelAndView();
		
    	 try {
			List<VehicleDto> listeVehicleDto = vehicleService.findAll();
			List<ClientDto> listeClientDto = clientService.findAll();
			outputModelAndView.setViewName("rents/create");
	        outputModelAndView.addObject("listeClients", listeClientDto);
	        outputModelAndView.addObject("listeVehicles", listeVehicleDto);
	        return outputModelAndView;
	        
		} catch (Exception e) {
			outputModelAndView.setViewName("rents/create");
			//Message d'erreur
			outputModelAndView.addObject("errorMessage", "Problème de récupération des informations nécessaires au formulaire");
			e.printStackTrace();
			return outputModelAndView;
		} 
        


    }

    //Projet 13/11
    //Ajout d'infos à l'utilisateur +  verif Business Rule Date de début < Date de fin (contrainte de validation fonctionnelle)
    @PostMapping
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    public String post(RedirectAttributes redirectAttributes, @ModelAttribute("rentCree") ReservationDto reservationDto) {

     
		try {
			reservationService.create(reservationDto);
			//Message d'information de succès
			redirectAttributes.addFlashAttribute("message", "Réservation créée!");

	        //Renvoi vers la page réservation
	        return "redirect:/rents/list";
		}
		catch (ValidationFonctionnelleException e) {
			//Message d'information d'erreur
			redirectAttributes.addFlashAttribute("errorMessage", "La date de début doit se situer <u>avant</u> la date de fin");
			e.printStackTrace();
			 //Renvoi vers la page réservation
			return "redirect:/rents/create";
		} 
		catch (Exception e) {
			//Message d'information d'erreur
			redirectAttributes.addFlashAttribute("errorMessage", "Problème lors de la création de la réservation!");
			e.printStackTrace();
			 //Renvoi vers la page réservation
			return "redirect:/rents/list";
		} 
        
    }


}
