package com.ensta.rentmanager.ui.controller.vehicle;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;


//Projet 13/11
@Controller
@RequestMapping("vehicles/details")
public class VehicleDetailController {
	
	private ReservationService reservationService;
	private VehicleService vehicleService;

    public VehicleDetailController(VehicleService vehicleService, ReservationService reservationService) {

        this.reservationService = reservationService;
        this.vehicleService = vehicleService;

    }
    
    
    @GetMapping
    public ModelAndView get(@ModelAttribute("vehicleDetailId") final int id) {
    	
    	try {
    		
			VehicleDto vehicleDto = vehicleService.findById(id);
			List<ReservationDto> listeReservationDto = reservationService.findResaByVehicleId(id); 
			
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("vehicles/details");
	        outputModelAndView.addObject("vehicule",vehicleDto );
	        outputModelAndView.addObject("listeReservation",listeReservationDto );
	        
	        return outputModelAndView;

			
		} catch (Exception e) {
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("vehicles/details");
	        outputModelAndView.addObject("errorMessage", "Problème de récupération des détails");
	        e.printStackTrace();
	        return outputModelAndView;
			
		} 
    	
    }
    
    

}