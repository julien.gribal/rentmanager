package com.ensta.rentmanager.ui.controller.vehicle;

import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.VehicleService;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("vehicles/create")
public class VehicleCreateController {

    private VehicleService vehicleService;

    // 19.10 modif suite TP6.2
    // public VehicleCreateController(VehicleService vehicleService) {
    //    this.vehicleService = vehicleService;
    // }
    
    private ClientService clientService;


    public VehicleCreateController(ClientService clientService,VehicleService vehicleService) {
        this.vehicleService = vehicleService;
 
        this.clientService = clientService;

    }

    //Projet 13/11
    //Modifs proprio/véhicule
    @GetMapping
    public ModelAndView get() {

    	
    	ModelAndView outputModelAndView = new ModelAndView();
    	
		try {
			List<ClientDto> listeClientDto = clientService.findAll();
			
			outputModelAndView.setViewName("vehicles/create");
	        outputModelAndView.addObject("listeClients", listeClientDto);
	        return outputModelAndView;
		} catch (Exception e) {
			outputModelAndView.setViewName("vehicles/create");
			outputModelAndView.addObject("errorMessage", "Problème de récupération de la liste des Clients");
			e.printStackTrace();
	        return outputModelAndView;
			
		} 
        

    }

    /* ------------------------------- TP6.2 19.10 : remplacement par DTO -----------------------------------
    @PostMapping
    public String post(@ModelAttribute("vehicleCree") Vehicle vehicle) throws DaoException, SQLException, ServiceException {

        vehicleService.create(vehicle);
        System.out.println(vehicle);
        return "redirect:/home";
    }

        ----------------------------------------- FIN -----------------------------------------------------*/

    //Projet 13/11
    //Ajout de messages et interception d'erreurs
    @PostMapping
    public String post(RedirectAttributes redirectAttributes, @ModelAttribute("vehicleCree") VehicleDto vehicleDto) {

        try {
        	vehicleService.create(vehicleDto);
			redirectAttributes.addFlashAttribute("message", "Nouveau véhicule créé!");
			
	        return "redirect:/vehicles/list";
		
        } catch (Exception e) {
        	redirectAttributes.addFlashAttribute("errorMessage", "Problème lors de la création du véhicule.");
			e.printStackTrace();
			return "redirect:/vehicles/list";
		} 
        
    }
    
    

}
