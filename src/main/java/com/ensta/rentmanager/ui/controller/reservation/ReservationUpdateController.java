package com.ensta.rentmanager.ui.controller.reservation;

import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.exception.ValidationFonctionnelleException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;

@Controller
@RequestMapping("rents/update")
public class ReservationUpdateController {

	private ReservationService reservationService;
    private VehicleService vehicleService;
    private ClientService clientService;
    

	//Onstocke l'id du client à updater côté serveur
    private int reservationId;


    public ReservationUpdateController(ReservationService reservationService, VehicleService vehicleService, ClientService clientService) {
        this.reservationService = reservationService;
        this.vehicleService = vehicleService;
        this.clientService = clientService;

    }

   

    @GetMapping
    public ModelAndView get(@ModelAttribute("selectedReservationId") final int id) {

    	//On sauve l'id du client pour l'update éventuel
    	this.reservationId = id;
    	
    	ModelAndView outputModelAndView = new ModelAndView();
    	
    	try {
    		ReservationDto reservationDto = reservationService.findById(id);
    		List<VehicleDto> listeVehicleDto = vehicleService.findAll();
			List<ClientDto> listeClientDto = clientService.findAll();
			outputModelAndView.setViewName("rents/update");
			outputModelAndView.addObject("reservation", reservationDto);
	        outputModelAndView.addObject("listeClients", listeClientDto);
	        outputModelAndView.addObject("listeVehicles", listeVehicleDto);
	        return outputModelAndView;

			
		} catch (Exception e) {
			outputModelAndView.setViewName("rents/update");
			//Message d'erreur
			outputModelAndView.addObject("errorMessage", "Problème de récupération des informations nécessaires au formulaire");
			e.printStackTrace();
			return outputModelAndView;
			
		} 
    	
    }

    

  //Projet 13/11
    //Ajout d'infos à l'utilisateur +  verif Business Rule Date de début < Date de fin (contrainte de validation fonctionnelle)
    @PostMapping
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    public String post(RedirectAttributes redirectAttributes, @ModelAttribute("rentUpdate") ReservationDto reservationDto) {

    	 reservationDto.setId(reservationId);
     
		try {
			reservationService.update(reservationDto);
			//Message d'information de succès
			redirectAttributes.addFlashAttribute("message", "Réservation mise à jour!");
	        //Renvoi vers la page réservation
	        return "redirect:/rents/list";
		}
		catch (ValidationFonctionnelleException e) {
			//Message d'information d'erreur
			redirectAttributes.addFlashAttribute("errorMessage", "La date de début doit se situer <u>avant</u> la date de fin");
			e.printStackTrace();
			//retour à la vue update en repassant l'id client
			redirectAttributes.addFlashAttribute("reservationUpdateId",reservationDto.getId());
			 //Renvoi vers la page réservation
			return "redirect:/rents/update";
		} 
		catch (Exception e) {
			//Message d'information d'erreur
			redirectAttributes.addFlashAttribute("errorMessage", "Problème lors de la création de la réservation!");
			e.printStackTrace();
			//retour à la vue update en repassant l'id client
			redirectAttributes.addFlashAttribute("reservationUpdateId",reservationDto.getId());
			 //Renvoi vers la page réservation
			return "redirect:/rents/update";
		} 
        
    }


}


