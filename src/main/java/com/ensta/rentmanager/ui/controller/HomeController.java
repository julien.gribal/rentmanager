package com.ensta.rentmanager.ui.controller;

import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.ReservationService;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
public class HomeController {

    private VehicleService vehicleService;
    private ClientService clientService;
    private ReservationService reservationService;

    public HomeController(VehicleService vehicleService, ClientService clientService, ReservationService reservationService) {
        this.vehicleService = vehicleService;
        this.clientService = clientService;
        this.reservationService = reservationService;
    }
    
    //Projet 13/11
    //Ajout d'un message en cas de pb
    @GetMapping
    public ModelAndView get() {
    	
    	ModelAndView outputModelAndView = new ModelAndView();
		
    	try {
			int countVehicles = vehicleService.count();
			int countClients = clientService.count();
	        int countReservations = reservationService.count();
	        
	        outputModelAndView.setViewName("home");
	        outputModelAndView.addObject("countVehicles", countVehicles);
	        outputModelAndView.addObject("countClients", countClients);
	        outputModelAndView.addObject("countReservations", countReservations);
	        return outputModelAndView;
		} catch (Exception e) {
			 outputModelAndView.setViewName("home");
			 //Message
			 outputModelAndView.addObject("errorMessage", "Problème détecté ");
			 e.printStackTrace();
			 return outputModelAndView;
		} 
        

       
    }

    

}
