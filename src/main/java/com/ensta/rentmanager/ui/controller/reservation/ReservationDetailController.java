package com.ensta.rentmanager.ui.controller.reservation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.service.ReservationService;


//Projet 13/11
@Controller
@RequestMapping("rents/details")
public class ReservationDetailController {
	
	private ReservationService reservationService;
	

    public ReservationDetailController(ReservationService reservationService) {

        this.reservationService = reservationService;
    }
    
    
    @GetMapping
    public ModelAndView get(@ModelAttribute("reservationDetailId") final int id) {
    	
    	try {
    		
			ReservationDto reservationDto = reservationService.findById(id);
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("rents/details");
	        outputModelAndView.addObject("reservation", reservationDto);
	        return outputModelAndView;

			
		} catch (Exception e) {
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("rents/details");
	        outputModelAndView.addObject("errorMessage", "Problème de récupération des détails");
	        e.printStackTrace();
	        return outputModelAndView;
			
		} 
    	
    }
    
    

}