package com.ensta.rentmanager.ui.controller.client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.exception.ValidationFonctionnelleException;
import com.ensta.rentmanager.exception.ValidationTechniqueException;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.utils.IOUtils;

@Controller
@RequestMapping("users/create")
public class ClientCreateController {

    private ClientService clientService;


    public ClientCreateController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    public ModelAndView get() {

        ModelAndView outputModelAndView = new ModelAndView();
        outputModelAndView.setViewName("users/create");
        return outputModelAndView;

    }

    //Projet 13/11
    //Ajout de messages d'information à l'utilisateur
    //Check e-mail
    @PostMapping
    public String post(RedirectAttributes redirectAttributes, @ModelAttribute("userCree") ClientDto clientDto) {

        String email = clientDto.getEmail();
        
        try {
        	//Tentative de création
        	if (!IOUtils.checkEmail(email)) throw new ValidationTechniqueException("regleMail");
			clientService.create(clientDto);
			//Information de succès - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("message", "Nouveau client créé!");
			//retour à la vue
			return "redirect:/users/list";
			
		} 
        catch (ValidationTechniqueException e) {
			//Catch erreur de validation technique du contrôleur sur l'adresse mail
        	//Information d'échec - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("errorMessage", "L'adresse mail doit contenir un @ et se terminer par .fr ou .com");
			 e.printStackTrace();
			//retour à la vue
			return "redirect:/users/create";
        }
        catch (ValidationFonctionnelleException e) {
			//Catch erreur, qui si elle intervient ici, est consécutive à la seule règle fonctionnelle sur le client
        	//Il doit être agé de plus de 18 ans.  La verif et le throw se font dans la couche service.
        	//Information d'échec - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("errorMessage", "Le client doit avoir plus de 18 ans");
			 e.printStackTrace();
			//retour à la vue
			return "redirect:/users/create";
        }
        catch (Exception e) {
			//Catch de toute erreur
        	//Information d'échec - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("errorMessage", "Le client n'a pu être créé!");
			 e.printStackTrace();
			//retour à la vue
			return "redirect:/users/list";
		}
        
    }


}
