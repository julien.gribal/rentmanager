package com.ensta.rentmanager.ui.controller.vehicle;

import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.service.VehicleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/vehicles/list")
public class VehicleListController {

    private VehicleService vehicleService;


    public VehicleListController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;

    }

    //Projet 13/11
    //Ajout d'info si le finAll() génère une exception
    @GetMapping
    public ModelAndView get() {

        ModelAndView outputModelAndView = new ModelAndView();
		try {
			List<VehicleDto> listeVehicleDto = vehicleService.findAll();
			outputModelAndView.setViewName("vehicles/list");
	        outputModelAndView.addObject("listeVehicles", listeVehicleDto);
	        return outputModelAndView;
	        
		} catch (Exception e) {
			outputModelAndView.setViewName("vehicles/list");
			//Message d'eereur
	        outputModelAndView.addObject("errorMessage", "La liste des véhicules n'a pu être récupérée");
	        e.printStackTrace();
	        return outputModelAndView;
		} 

      
        

    }
    
    //Projet 13/11
    //Méthode recevant en paramètre "id du client" afin de pouvoir le supprimer
    @RequestMapping(method = RequestMethod.GET, params = {"selectedVehicule"})
    public String delete(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedVehicule", required = false) int selectedVehicule) {

        try {
        	//Tentative de suppression
			vehicleService.delete(selectedVehicule);
			//Information de succès - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("message", "Véhicule n°" + selectedVehicule + " supprimé!");
			//Retour sur la page 
			return "redirect:/vehicles/list";
		} catch (Exception e) {
			//Catch de toute erreur
			 //Information de succès - ajout d'un attribut à la vue
			 redirectAttributes.addFlashAttribute("errorMessage", "Un problème est survenu lors de la suppression du véhicule n°"+selectedVehicule+" !");
			 e.printStackTrace();
			 //Retour sur la page
			 return "redirect:/vehicles/list";
		} 

       
    
    }
    
  //Projet 13/11
    //Méthode recevant en paramètre "id du vehicule" afin d'avoir les détails
    @RequestMapping(method = RequestMethod.GET, params = {"selectedVehicleDetails"})
    public String showDetails(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedVehicleDetails", required = false) int selectedVehicleDetails) {
    	
    	  	redirectAttributes.addFlashAttribute("vehicleDetailId", selectedVehicleDetails);
    	  	return "redirect:/vehicles/details";
		
       
   }
    
  //Projet 13/11
    //Méthode recevant en paramètre "id du véhicule" afin de faire un update
    @RequestMapping(method = RequestMethod.GET, params = {"selectedVehicleUpdate"})
    public String update(RedirectAttributes redirectAttributes, @RequestParam(value = "selectedVehicleUpdate", required = false) int selectedVehicleUpdate) {
    	
    	  	redirectAttributes.addFlashAttribute("vehicleUpdateId", selectedVehicleUpdate);
    	  	return "redirect:/vehicles/update";
		
   }

    
}
