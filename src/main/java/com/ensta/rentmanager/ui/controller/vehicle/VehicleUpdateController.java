package com.ensta.rentmanager.ui.controller.vehicle;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.VehicleService;

@Controller
@RequestMapping("vehicles/update")
public class VehicleUpdateController {

	private ClientService clientService;
	private VehicleService vehicleService;

    
	//Onstocke l'id du client à updater côté serveur
    private int vehicleId;

    public VehicleUpdateController(ClientService clientService, VehicleService vehicleService) {
        this.clientService = clientService;
        this.vehicleService = vehicleService;
   
    }

    @GetMapping
    public ModelAndView get(@ModelAttribute("vehicleUpdateId") final int id) {

    	//On sauve l'id du client pour l'update éventuel
    	this.vehicleId = id;
    	
    	try {
    		
			VehicleDto vehicleDto = vehicleService.findById(id);
			List<ClientDto> listeClientDto = clientService.findAll();
			
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("vehicles/update");
	        outputModelAndView.addObject("vehicule",vehicleDto );
	        outputModelAndView.addObject("listeClients", listeClientDto);
	        return outputModelAndView;

			
		} catch (Exception e) {
			ModelAndView outputModelAndView = new ModelAndView();
	        outputModelAndView.setViewName("vehicles/update");
	        outputModelAndView.addObject("errorMessage", "Problème de récupération des détails");
	        e.printStackTrace();
	        return outputModelAndView;
			
		} 
    	
    }

    

    //Projet 13/11
    //Ajout de messages d'information à l'utilisateur
    //Check e-mail
    @PostMapping
    public String post(RedirectAttributes redirectAttributes, @ModelAttribute("vehicleUpdate") VehicleDto vehicleDto) {

        vehicleDto.setId(vehicleId);
        
        try {
        	//Tentative de création
        	vehicleService.update(vehicleDto);
			//Information de succès - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("message", "Véhicule mis à jour!");
			//retour à la vue
			return "redirect:/vehicles/list";
			
		} 
        catch (Exception e) {
			//Catch de toute erreur
        	//Information d'échec - ajout d'un attribut à la vue
			redirectAttributes.addFlashAttribute("errorMessage", "Le véhicule n'a pu être mis à jour!");
			 e.printStackTrace();
			//retour à la vue update en repassant l'id client
			redirectAttributes.addFlashAttribute("vehiculeUpdateId",vehicleDto.getId());
			return "redirect:/vehicles/update";
		}
        
    }


}


