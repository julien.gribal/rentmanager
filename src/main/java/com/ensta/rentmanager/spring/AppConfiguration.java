package com.ensta.rentmanager.spring;

import org.h2.jdbcx.JdbcDataSource;
import org.hibernate.Session;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.sql.DataSource;

@Configuration                                                               
@ComponentScan({"com.ensta.rentmanager.service","com.ensta.rentmanager.dao", "com.ensta.rentmanager.dto", "com.ensta.rentmanager.mapper"})
public class AppConfiguration {
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.ensta.rentmanager.model"); 
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        // dataSource.setURL("jdbc:h2:~/h2.database/RentManagerDatabase/RentManagerDatabase");
        dataSource.setURL("jdbc:h2:~/h2.database/RentManagerDatabase"); // 19.10 : modification lien
        dataSource.setUser("");
        dataSource.setPassword("");

        return dataSource;
    }

    @Bean
    public Session getSession(LocalSessionFactoryBean localSessionFactoryBean) {
        return localSessionFactoryBean.getObject().openSession();
    }
}
