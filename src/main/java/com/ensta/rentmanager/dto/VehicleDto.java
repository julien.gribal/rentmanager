package com.ensta.rentmanager.dto;

import com.ensta.rentmanager.model.Client;

public class VehicleDto {

    private int id ;
    private String constructeur ;
    private String modele ;
    private int nb_places ;
    private Client client;

   
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public VehicleDto() {

    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getConstructeur() {
        return constructeur;
    }
    public void setConstructeur(String constructeur) {
        this.constructeur = constructeur;
    }
    public String getModele() {
        return modele;
    }
    public void setModele(String modele) {
        this.modele = modele;
    }
    public int getNb_places() {
        return nb_places;
    }
    public void setNb_places(int nb_places) {
        this.nb_places = nb_places;
    }
    
    @Override
	public String toString() {
		return "Vehicle [id=" + id + ", proprietaire=" + client.getId() + ", constructeur=" + constructeur + ", modele=" + modele + ", nb_places=" + nb_places + "]";
	}

}
