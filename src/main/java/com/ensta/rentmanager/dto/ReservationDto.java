package com.ensta.rentmanager.dto;

import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicle;

public class ReservationDto {

    private int id ;

    private Client client;
    private Vehicle vehicle;
    private String debut;   
    private String fin;     

    public ReservationDto() {

    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public String getDebut() {
        return debut;
    }
    public void setDebut(String debut) {
        this.debut = debut;
    }
    public String getFin() {
        return fin;
    }
    public void setFin(String fin) {
        this.fin = fin;
    }

    @Override
	public String toString() {
		return "Reservation [id=" + id + ", client=" + client.getId() + ", vehicle=" + vehicle.getId() + ", debut=" + debut + ", fin="
				+ fin + "]";
	}


}
