package com.ensta.rentmanager.mapper;

import com.ensta.rentmanager.dto.ClientDto;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.utils.IOUtils;

import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class ClientMapper {

    public Client mapToEntity(ClientDto clientDto) {

        Client client = new Client();
        client.setId(clientDto.getId());
        client.setNom(clientDto.getNom());
        client.setPrenom(clientDto.getPrenom());
        client.setEmail(clientDto.getEmail());
        client.setNaissance(IOUtils.readDate(clientDto.getNaissance()));

        return client;
    }

    public ClientDto mapToDto(Client client) {
        ClientDto clientDto = new ClientDto();
        clientDto.setId(client.getId());
        clientDto.setNom(client.getNom());
        clientDto.setPrenom(client.getPrenom());
        clientDto.setEmail(client.getEmail());
        clientDto.setNaissance(client.getNaissance().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        return clientDto;
    }

}
