package com.ensta.rentmanager.mapper;

import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

import com.ensta.rentmanager.utils.IOUtils;
import org.springframework.stereotype.Component;

import com.ensta.rentmanager.dto.ReservationDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Reservation;
import com.ensta.rentmanager.service.ClientService;
import com.ensta.rentmanager.service.VehicleService;

@Component
public class ReservationMapper {

    private VehicleService vehicleService;
    private ClientService clientService;
    private ClientMapper clientMapper;
    private VehicleMapper vehicleMapper;

    public ReservationMapper(VehicleService vehicleService, ClientService clientService, ClientMapper clientMapper, VehicleMapper vehicleMapper) {
        this.vehicleService = vehicleService;
        this.clientService = clientService;
        this.clientMapper = clientMapper;
        this.vehicleMapper = vehicleMapper;
    }

    public Reservation mapToEntity(ReservationDto reservationDto) throws ServiceException, DaoException, SQLException {

        Reservation reservation = new Reservation();
        reservation.setId(reservationDto.getId());
        reservation.setClient(clientMapper.mapToEntity(clientService.findById(reservationDto.getClient().getId())));
        reservation.setVehicle(vehicleMapper.mapToEntity(vehicleService.findById(reservationDto.getVehicle().getId())));
        reservation.setDebut(IOUtils.readDate(reservationDto.getDebut())); 
        reservation.setFin(IOUtils.readDate(reservationDto.getFin()));   

        return reservation;
    }

    public ReservationDto mapToDto(Reservation reservation) {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(reservation.getId());
        reservationDto.setClient(reservation.getClient());
        reservationDto.setVehicle(reservation.getVehicle());
        reservationDto.setDebut(reservation.getDebut().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        reservationDto.setFin(reservation.getFin().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));     

        return reservationDto;
    }

}
