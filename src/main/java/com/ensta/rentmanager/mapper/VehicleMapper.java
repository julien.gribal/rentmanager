package com.ensta.rentmanager.mapper;

import java.sql.SQLException;

import org.springframework.stereotype.Component;

import com.ensta.rentmanager.dto.VehicleDto;
import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.service.ClientService;

@Component
public class VehicleMapper {
	

	private ClientService clientService;
	private ClientMapper clientMapper;

    public VehicleMapper(ClientService clientService, ClientMapper clientMapper) {
        
        this.clientService = clientService;
        this.clientMapper = clientMapper;
    }

    public Vehicle mapToEntity(VehicleDto vehicleDto) throws ServiceException, DaoException, SQLException {
        Vehicle vehicle = new Vehicle();
        vehicle.setId(vehicleDto.getId());
        vehicle.setConstructeur(vehicleDto.getConstructeur());
        vehicle.setModele(vehicleDto.getModele());
        vehicle.setNb_places(vehicleDto.getNb_places());
        vehicle.setClient(clientMapper.mapToEntity(clientService.findById(vehicleDto.getClient().getId())));
        
        return vehicle;

    }

    public VehicleDto mapToDto(Vehicle vehicle) {
        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setId(vehicle.getId());
        vehicleDto.setConstructeur(vehicle.getConstructeur());
        vehicleDto.setModele(vehicle.getModele());
        vehicleDto.setNb_places(vehicle.getNb_places());
        vehicleDto.setClient(vehicle.getClient());
        return vehicleDto;

    }
}
