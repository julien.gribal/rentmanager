package com.ensta.rentmanager.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class IOUtils {
	//ancienne instruction : private static Scanner scanner = new Scanner(System.in);
	//Mise en place du scanner comme attribut static.  Il y en a qu'un qui 
	//reste ouvert pour la dur�e de vie de l'appli
	static Scanner scanner = new Scanner(System.in);
	
	/**
	 * Affiche un message sur la sortie standard
	 * @param message
	 */
	public static void print(String message) {
		System.out.println(message); 
	}
	
	/**
	 * Affiche un message sur la sortie standard
	 * @param message
	 * @param mandatory
	 */
	public static String readString(String message, boolean mandatory) {
		print(message);
		
		String input = null;
		int attempt = 0;
		
		do {
			if (attempt >= 1) {
				print("Cette valeur est obligatoire");
			}
			
			input = readString();
			attempt++;
		} while (mandatory && (input.isEmpty() || input == null));
		
		return input;
	}
	
	/**
	 * Lit un message sur l'entrée standard
	 */
	public static String readString() {
		// Ancienne instruction 18/09/19
		/*
		 * String value = "" ; if (scanner.hasNextLine()) { value = scanner.nextLine(); }
		 */
		// Ancienne instruction 12/09/19
		//Scanner scanner = new Scanner(System.in);
		String value = scanner.nextLine();
		//scanner.close();
		return value;
	}
	
	/**
	 * Lit un entier sur l'entrée standard
	 * @param message
	 * @return
	 */
	public static int readInt(String message) {
		print(message);
		
		String input = null;
		int output = 0;
		boolean error = false;
		
		do {
			input = readString();
			error = false;
						
			try {
				output = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				error = true;
				print("Veuillez saisir un nombre");
			}
		} while (error);
		
		return output;
	}
	
	/**
	 * Lit une date sur l'entrée standard
	 * @param message
	 * @param mandatory
	 * @return
	 */
	

	/*
	public static LocalDate readDate(String message, boolean mandatory) {
	print(message);

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	LocalDate output = null; boolean error = false;

	do { try { error = false; String stringDate = readString(); output =
	LocalDate.parse(stringDate, formatter); } catch (DateTimeParseException e) {
	error = true; print("Veuillez saisir une date valide (dd/MM/yyyy)"); } }
	while (error && mandatory);

	return output; }


	 */
	
	/*
	 * J'ai re-modifi� leur m�thode  readDate() dans IOUtils.   (ancienne m�thode 18/09/19 plus haut)
	 * Ca sert � rien de partir � nouveau dans du readLine() puisqu'on passe d�j� un String en param�tre.
	 * j'ai tout comment� pour ne garder que output = LocalDate.parse(message, formatter);
	 * (le message est d�j� fourni !)
	 */
	public static LocalDate readDate(String message) throws DateTimeParseException {
		//print(message);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate output = LocalDate.parse(message, formatter);
	        
		return output;
	}
	
	
	//Projet 13/11
	//Verif adresse mail contient au moins un @ et uniquement un seul
	//Verif que le mail se termine par .com ou .fr
	public static boolean checkEmail (String email)  {
		//D'abord l'arobase
		boolean arobaseOK = false;
		
		int firstIndex = email.indexOf("@");
		//Si l'index est >=0 alors il y a au moins un @
		//Le dernier index trouvé doit être le même que le premier 
		if( firstIndex >=0 && firstIndex == email.lastIndexOf("@")) {
			arobaseOK = true;
		}
		
		//Puis l'extension .com ou .fr
		boolean extensionOK = false;
		//D'abord s'assurer que l'email fait au moins 4 caractères pour éviter les erreurs de email.length()
		if (email.length() >= 4) {
			if (email.substring(email.length() - 4).equals(".com") || email.substring(email.length()-3).equals(".fr")){
				extensionOK = true;
			}
		}
		
		return arobaseOK && extensionOK;
		
	}
	
	//Projet 13/11
	//Verif Age
	public static int calculAge(LocalDate dateNaissance) {
		
		LocalDate aujourdhui = LocalDate.now();
		return Period.between(dateNaissance, aujourdhui).getYears();
		
	}
	
	//Projet 13/11
	//Verif begda <= endda
	//Spéciale dédicace Walldorf
	public static boolean verifZ (String begda, String endda) {
		return readDate(begda).isBefore(readDate(endda));
		
	}

}

	