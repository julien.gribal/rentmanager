package com.ensta.rentmanager.model;

import java.time.LocalDate ;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Reservation")
public class Reservation {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	int id ;
	
	@ManyToOne
    @JoinColumn(name="CLIENT_ID")
	private Client client; 
	
	@ManyToOne
	@JoinColumn(name="VEHICULE_ID")
	private Vehicle vehicle ;
	
	private LocalDate debut ;
	private LocalDate fin ;

	public Reservation() {

	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public LocalDate getDebut() {
		return debut;
	}
	public void setDebut(LocalDate debut) {
		this.debut = debut;
	}
	public LocalDate getFin() {
		return fin;
	}
	public void setFin(LocalDate fin) {
		this.fin = fin;
	}
	@Override
	public String toString() {
		return "Reservation [id=" + id + ", debut=" + debut + ", fin="
				+ fin + ", client=" +client.getId()+", vehicle="+vehicle.getId()+"]";
	}



}
