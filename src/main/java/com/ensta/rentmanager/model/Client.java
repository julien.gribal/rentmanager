package com.ensta.rentmanager.model;

import java.time.LocalDate ;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table (name="Client")
public class Client {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id ;
	
	private String nom ;
	private String prenom ;
	private String email ;
	private LocalDate naissance ;
	
	@OneToMany(mappedBy = "client", orphanRemoval=true, cascade={CascadeType.REMOVE})
    private List<Reservation> listeReservations= new ArrayList<Reservation>();
  
	
    @OneToMany(mappedBy = "client", orphanRemoval=true, cascade={CascadeType.REMOVE})
    private List<Vehicle> listeVehicules= new ArrayList<Vehicle>();
	
	
	public Client() { 	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getNaissance() {
		return naissance;
	}

	public void setNaissance(LocalDate naissance) {
		this.naissance = naissance;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", naissance="
				+ naissance + "]";
	}


	public List<Reservation> getListeReservations() {
		return listeReservations;
	}


	public void setListeReservations(List<Reservation> listeReservations) {
		this.listeReservations = listeReservations;
	}


	public List<Vehicle> getListeVehicules() {
		return listeVehicules;
	}


	public void setListeVehicules(List<Vehicle> listeVehicules) {
		this.listeVehicules = listeVehicules;
	}

}
