package com.ensta.rentmanager.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Vehicule")
public class Vehicle {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id ;
	private String constructeur ;
	private String modele ;
	private int nb_places ;
	
	@ManyToOne
	@JoinColumn(name="CLIENT_ID", nullable=true)
	private Client client;
	
	@OneToMany(mappedBy = "vehicle", orphanRemoval=true, cascade={CascadeType.REMOVE})
	private List<Reservation> listeReservations= new ArrayList<Reservation>();
	
	public List<Reservation> getListeReservations() {
		return listeReservations;
	}

	public void setListeReservations(List<Reservation> listeReservations) {
		this.listeReservations = listeReservations;
	}

	public Vehicle() { 	};

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConstructeur() {
		return constructeur;
	}

	public void setConstructeur(String constructeur) {
		this.constructeur = constructeur;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getNb_places() {
		return nb_places;
	}

	public void setNb_places(int nb_places) {
		this.nb_places = nb_places;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", constructeur=" + constructeur + ", modele=" + modele + ", nb_places=" + nb_places + ", propriétaire="+client.getId()+"]";
	}

}
