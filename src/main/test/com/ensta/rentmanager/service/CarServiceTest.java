
package com.ensta.rentmanager.service;

        import static junit.framework.TestCase.assertTrue;
        import static org.junit.Assert.assertEquals;
        import static org.mockito.ArgumentMatchers.any;
        import static org.mockito.Mockito.when;

        import com.ensta.rentmanager.dao.ClientDao;
        import com.ensta.rentmanager.dao.VehicleDao;
        import com.ensta.rentmanager.exception.DaoException;
        import com.ensta.rentmanager.exception.ServiceException;
        import com.ensta.rentmanager.model.Client;
        import com.ensta.rentmanager.model.Vehicle;
        import org.junit.Before;
        import org.junit.Test;

        import org.junit.runner.RunWith;
        import org.mockito.InjectMocks;
        import org.mockito.Mock;
        import org.mockito.junit.MockitoJUnitRunner;

        import java.sql.SQLException;
        import java.util.ArrayList;
        import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    @InjectMocks
    VehicleService vehicleService;
    @Mock
    VehicleDao vehicleDao;
    Vehicle vehicleTest;
    Vehicle vehicleTestFail;
    List<Vehicle> vehicleList;
    @Before
    public void init() {
        this.vehicleTest = new Vehicle(1, "Peugeot", "205 GTI", 4);
        this.vehicleTestFail = new Vehicle(4, null, "véhicule zéro", 0);
        this.vehicleList = new ArrayList<>();
        this.vehicleList.add(new Vehicle(2, "Renault", "Super 5 GT", 2));
        this.vehicleList.add(new Vehicle(3, "Renault", "Alpine", 2));
    }
    @Test
    public void create_with_valid_vehicle_should_return_valid_id() throws DaoException, ServiceException, SQLException {
        when(vehicleDao.create(any())).thenReturn(1L);
        assertEquals(1L, vehicleService.create(this.vehicleTest));
    }
    @Test(expected = ServiceException.class)
    public void create_with_invalid_vehicle_should_return_exception() throws DaoException, ServiceException, SQLException {
        vehicleService.create(this.vehicleTestFail);
    }

 /*   @Test
    void dummy_test() {
        Assertions.assertTrue(true);
    } */
}
