## RentManager

##### Version 7.1 - 13/11/19

<i>* En italique : périmètre déjà traité dans une itération précédente.</i>

<u>Fait</u>

- Divers
  - Contrainte de validation technique sur l'adresse mail (<code>@</code> et <code>.com</code>/<code>.fr</code>)
  - Contrainte de validation fonctionnelle
    - Un utilisateur ne peut être ajouté que s'il est majeur
    - S'il est saisi, dateDébutRéservation <= dateFinRéservation
  - <code>pom.xml</code> et <code>web.xml</code> : amélioration de l'encodage et gestion "cross-platform" (Windows, macOS)
  - "Factorisation" du code JSP gérant les messages vers une page <i>ad hoc</i> <code>error.jsp></code>
  - BDD H2 : ajout de la colonne <code> VEHICULE/PROPRIETAIRE [FK]</code>
  - nettoyage du code et élimination de certaines documentations (reportées dans ce <code>readme</code>)
  - modification de l'architecture logicielle globale (voir PDF joint)
 
- [PFM]
  - <i> Véhicules </i>
    - <i>Afficher la liste des véhicules présents dans la BDD </i>
    - <i> Insérer un véhicule dans la BDD </i>
    - <i> Supprimer un véhicule </i>
  - <i> Clients
    - <i> Afficher la liste des clients présents dans la BDD </i>
    - <i> Insérer un client dans la BDD (conversion <code>String</code> vers <code>LocalDate</code>) </i>
    - <i> Supprimer un client </i>
  - <i> Page d'accueil </i>
    - <i> Afficher le nb de véhicules présents dans la BDD </i>
    - <i> Afficher le nb de clients   présents dans la BDD </i>
  - Messages (avec lib. JS + CSS Growl)
    - Client
      - <i> Suppression </i>
      - Création (+ respect des contraintes techniques et fonctionnelles)
      - Mise à jour
    - Réservation
      - <i> Suppression </i>
      - Création
      - Mise à jour
    - Voiture
      - <i> Suppression </i>
      - Création
      - Mise à jour
      
- [PFA]
  - <i> Réservations </i>
    - <i> Afficher la liste des réservations présentes dans la BDD </i>
    - <i> Insérer une réservation dans la BDD </i>
      - <i> Afficher la liste des véhicules dans une liste déroulante </i>
      - <i> Afficher la liste des clients dans une liste déroulante </i>
    - <i> Supprimer une réservation </i>
  - <i> Page d'accueil </i>
    - <i> Afficher le nombre de réservations présentes dans la BDD </i>
  - Créer la page de profil d'un client
    - Afficher le nombre de réservations d'un client
    - Afficher les réservations d'un client
  - Modifier la base de données pour lier une voiture à un client (son propriétaire)
  - Modifier le formulaire d'insertion de véhicule pour permettre à l'utilisateur de sélectionner le propriétaire d'un véhicule
  - Page de profil d'un client
    - Afficher le nombre de véhicules appartenant à ce client
    - Afficher les véhicules appartenant à ce client

- [PFA BONUS (obtenu par copier-coller-adapter...)]
  - Client
    - Mise à jour
  - Véhicule
    - Détails : propriétaire et réservations associées (liste et nombre)
    - Mise à jour
  - Réservations
    - Détails
    - Mise à jour
  - Apports sur la couche modèle et Hibernate / JPA (ex. : <code>@OneToMany(mappedBy = "client", orphanRemoval=true, cascade={CascadeType.REMOVE}</code>, voir couche modèle)
    - La suppression d'un client entraîne la suppression des véhicules dont il est propriétaire et des réservations associées à ce véhicule
    - La suppression d'un véhicule entraîne la suppression des réservations associées
    - complément : cela nécessite des commit explicites dans la couche DAO et la purge des <code>@Entity</code> de session

##### Version 7.0 - 12/11/19

Ajout d'une branche <code>archive_projet</code> permettant d'historiser la version 7 en vue des prochains commits.

##### Version 7.0 - 06/11/19

Fait
- [PFM]
  - Véhicules
    - Afficher la liste des véhicules présents dans la BDD
    - Insérer un véhicule dans la BDD
    - Supprimer un véhicule
  - Clients
    - Afficher la liste des clients présents dans la BDD
    - Insérer un client dans la BDD (conversion String vers Date dans ClientWebMapper)
  - Page d'accueil
    - Afficher le nb de véhicules présents dans la BDD
    - Afficher le nb de clients   présents dans la BDD
  - Messages (avec lib. JS + CSS Growl)
    - Client
      - Suppression
    - Réservation
      - Suppression
    - Véhicule
      - Suppression
- [PFA]
  - Réservations
    - Afficher la liste des réservations présentes dans la BDD
    - Insérer une réservation dans la BDD
      - Afficher la liste des véhicules dans une liste déroulante
      - Afficher la liste des clients dans une liste déroulante
    - Supprimer une réservation
  - Page d'accueil
    - Afficher le nombre de réservations présentes dans la BDD

@TODO
- Contrainte de validation technique
- Contrainte de validation fonctionnelle
  - Un utilisateur ne peut être ajouté que s'il est majeur
  - S'il est saisi, dateDébutRéservation <= dateFinRéservation
- [PFM]
  - Sur toutes ces pages, afficher un message d'erreur si qqch ne se passe pas comme prévu
- [PFA]
  - Créer la page de profil d'un client
    - Afficher le nombre de réservations d'un client
    - Afficher les réservations d'un client
  - Modifier la base de données pour lier une voiture à un client (son propriétaire)
  - Modifier le formulaire d'insertion de véhicule pour permettre à l'utilisateur de sélectionner le propriétaire d'un véhicule
  - Page de profil d'un client
    - Afficher le nombre de véhicules appartenant à ce client
    - Afficher les véhicules appartenant à ce client

  ##### Version 6.0 - 24/10/19
  
  - Correction de l'encodage (cf. Martine, "J'Ã©cris en UTF-8")
  - Suppression du dossier <i>/target</i> du push Git  